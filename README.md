# cool-activiti
### 
#### 介绍
1.  cool-activiti是一个SpringBoot集成activiti实现在创建、部署流程、复制流程、删除流程以及流程规则配置，实现请假流程工作流流转和业务处理，感兴趣可以Watch、Start持续关注项目最新状态
2.  **本项目只实现activit流程相关的业务与权限相关的用户角色菜单管理未做实现**
3. 增加通过页面动态控制定时任务的启动、暂停、创建、删除

##### 扩展
1.  提供了sql脚本 cool-activiti.sql


### 用户表核心字段讲解
1. 密码是123456
2. tag标识身份，noraml 普通员工，depart 部门经理  manager 总经理
3. org标识单位  A，B  只是一个不通字段用来标识部门，实际开发会存储部门id



### 模拟场景
1. activity 简单请假带驳回流程研究，静态指定用户Id审批
2. activity 根据用户身份请假流程不同，代码编写验证
3. activity 监听器实现审批人动态指定
4. activity 流程设计静态变量指定

资料参考：https://www.cnblogs.com/shyroke/p/8005066.html

### 目的
1.只是为了学习activ框架项目，愿意给star的给，不勉强


### 更新日志
1. 2021-07-07 添加了导入Bpmn模型,可以将bpmn文件直接导入到系统中

### 现有问题
1. 导入bpmn生成对应的流程图的逻辑还需要测试
2. 【导入bpmn】按钮有bug，有时候点击不了

### 原版为sunny-activiti
感兴趣可以在码云搜原版sunny-activiti,原版没有sql脚本，不过还是很感谢原版的支持

#### 软件架构
| 定位  | 技术栈               |
|-----|-------------------|
| 后端  | SpringBoot v2.3.0 、Activiti工作流|
| 数据库 | MySQL             |
| 数据库 | mybatis-plus      |
| 缓存 | redis      |
| 前端| layui、thymeleaf|
