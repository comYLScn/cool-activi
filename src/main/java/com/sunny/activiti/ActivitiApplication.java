package com.sunny.activiti;

import cn.hutool.crypto.SecureUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {org.activiti.spring.boot.SecurityAutoConfiguration.class,
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class })
public class ActivitiApplication {

    public static void main(String[] args) {
        System.out.println(SecureUtil.md5("123456"));
        SpringApplication.run(ActivitiApplication.class, args);
    }

}
