package com.sunny.activiti.controller;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sunny.activiti.common.entity.ResponseResult;
import com.sunny.activiti.common.entity.ResponseTableResult;
import com.sunny.activiti.common.entity.ResponseUtil;
import com.sunny.activiti.common.entity.ResultCode;
import com.sunny.activiti.common.flow.cmd.HistoryProcessInstanceDiagramCmd;
import com.sunny.activiti.entity.FlowDef;
import com.sunny.activiti.service.IFlowInfoService;
import com.sunny.activiti.service.IProcesService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.Process;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.*;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.util.io.InputStreamSource;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.image.impl.DefaultProcessDiagramGenerator;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: ActivitiModelController
 * @Description: 工作流引擎控制器
 * @Author: sunt
 * @Date: 2019/7/211:07
 **/
@Controller
@RequestMapping("model")
@Slf4j
public class ActivitiModelController {

    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private IProcesService procesService;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private IFlowInfoService flowInfoService;
    @Autowired
    private ManagementService managementService;

    /**
     * 1、【创建模型】，点击新建流程，进入此方法
     *  repositoryService.saveModel(modelData);
     *  repositoryService.addModelEditorSource
     * @param request
     * @param response
     */
    @RequestMapping("/createModel")
    public void createModel(HttpServletRequest request, HttpServletResponse response) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode editorNode = objectMapper.createObjectNode();
            editorNode.put("id", "canvas");
            editorNode.put("resourceId", "canvas");
            ObjectNode stencilSetNode = objectMapper.createObjectNode();
            stencilSetNode.put("namespace", "http://b3mn.org/stencilset/bpmn2.0#");
            editorNode.put("stencilset", stencilSetNode);
            Model modelData = repositoryService.newModel();

            ObjectNode modelObjectNode = objectMapper.createObjectNode();
            modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME, "name");
            modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, 1);
            modelObjectNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION, "description");
            modelData.setMetaInfo(modelObjectNode.toString());
            modelData.setName("name");
            modelData.setKey(StringUtils.defaultString("key"));

            repositoryService.saveModel(modelData);
            repositoryService.addModelEditorSource(modelData.getId(), editorNode.toString().getBytes("utf-8"));

            request.setAttribute("modelId", modelData.getId());

            response.sendRedirect(request.getContextPath() + "/modeler.html?modelId=" + modelData.getId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 2、 【编辑模型】 编辑流程
     * 1、编辑模型
     * 2、保存资源
     *
     *  repositoryService.saveModel(model);
     *  repositoryService.addModelEditorSource
     *  repositoryService.addModelEditorSourceExtra
     *
     * @Author sunt
     * @Description 保存流程
     * @Date 11:42 2019/7/2
     * @Param [modelId, name, json_xml, svg_xml, description]
     * @return void
     **/
    @PutMapping(value = { "/{modelId}/save" })
    @ResponseStatus(HttpStatus.OK)
    public void saveModel(@PathVariable String modelId, @RequestParam("name") String name,
                          @RequestParam("json_xml") String json_xml, @RequestParam("svg_xml") String svg_xml,
                          @RequestParam("description") String description) {
        try {
            Model model = this.repositoryService.getModel(modelId);

            ObjectNode modelJson = (ObjectNode) this.objectMapper.readTree(model.getMetaInfo());

            modelJson.put("name", name);
            modelJson.put("description", description);
            model.setMetaInfo(modelJson.toString());
            model.setName(name);

            this.repositoryService.saveModel(model);

            this.repositoryService.addModelEditorSource(model.getId(),json_xml.getBytes("utf-8"));

            InputStream svgStream = new ByteArrayInputStream(svg_xml.getBytes("utf-8"));
            TranscoderInput input = new TranscoderInput(svgStream);

            PNGTranscoder transcoder = new PNGTranscoder();

            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            TranscoderOutput output = new TranscoderOutput(outStream);

            transcoder.transcode(input, output);
            byte[] result = outStream.toByteArray();
            this.repositoryService.addModelEditorSourceExtra(model.getId(), result);
            outStream.close();
        } catch (Exception e) {
            throw new ActivitiException("Error saving model", e);
        }
    }


    /**
     * repositoryService.createModelQuery().orderByCreateTime().desc().listPage
     * 表act_re_model
     * 流程列表查询
     * @return
     */
    @RequestMapping("queryModelList")
    @ResponseBody
    public ResponseTableResult<List<Model>> queryModelList(HttpServletRequest request) {
        int pageNo = Integer.valueOf(request.getParameter("page"));
        int pageSize = Integer.valueOf(request.getParameter("limit"));
        int firstResult = (pageNo-1)*pageSize;
        long count = repositoryService.createModelQuery().count();
        List<Model> list = repositoryService.createModelQuery().orderByCreateTime().desc().listPage(firstResult,pageSize);
        return ResponseUtil.makeTableRsp(0,count,list);
    }




    /**
     * 部署流程
     *  repositoryService.createDeployment().name(modelData.getName()).addString(processName, new String(bpmnBytes,"utf-8")).deploy()  部署
     *  repositoryService.saveModel(modelData);  更新部署id
     *  flowInfoService.insertFlowDef(flowDef); 流程定义表，插入流程id 、流程名称
     * @param request
     * @param redirectAttributes
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "deployModel")
    @ResponseBody
    public ResponseResult<String> deployModel(HttpServletRequest request, RedirectAttributes redirectAttributes) throws IOException {
        String modelId = request.getParameter("modelId");
        if (StringUtils.isNoneBlank(modelId)) {
            Model modelData = this.repositoryService.getModel(modelId);
            ObjectNode modelNode = (ObjectNode) new ObjectMapper().readTree(repositoryService.getModelEditorSource(modelData.getId()));
            byte[] bpmnBytes = null;

            BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
            bpmnBytes = new BpmnXMLConverter().convertToXML(model);
            String processName = modelData.getName() + ".bpmn20.xml";
            Deployment deployment = repositoryService.createDeployment().name(modelData.getName()).addString(processName, new String(bpmnBytes,"utf-8")).deploy();
            modelData.setDeploymentId(deployment.getId());
            repositoryService.saveModel(modelData);
            redirectAttributes.addFlashAttribute("message", "部署成功，部署ID=" + deployment.getId());
            //向流程定义表保存数据
            FlowDef flowDef = new FlowDef();
            List<Process> processes = model.getProcesses();
            for (Process process : processes) {
                flowDef.setFlowCode(process.getId());
                flowDef.setFlowName(process.getName());
            }
            flowInfoService.insertFlowDef(flowDef);

            return ResponseUtil.makeOKRsp("部署成功");
        }
        return ResponseUtil.makeErrRsp(ResultCode.NOT_FOUND.code,"系统异常,流程ID不存在");
    }

    /**
     * 删除流程
     * @param request
     * @return
     */
    @RequestMapping("delModel")
    @ResponseBody
    public ResponseResult<String> delModel(HttpServletRequest request) {
        String modelId = request.getParameter("modelId");
        if(StrUtil.isBlank(modelId)) {
            return ResponseUtil.makeErrRsp(ResultCode.NOT_FOUND.code,"流程ID不存在!");
        }
        repositoryService.deleteModel(modelId);
        return ResponseUtil.makeOKRsp("删除流程成功!");
    }

    /**
     * 复制流程
     * @param modelId
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping("copyModel")
    public void copyModel(String modelId, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Model modelData = repositoryService.newModel();
        Model oldModel = repositoryService.getModel(modelId);
        modelData.setName(oldModel.getName() + "-复制");
        modelData.setKey(oldModel.getKey());
        modelData.setMetaInfo(oldModel.getMetaInfo());
        repositoryService.saveModel(modelData);
        repositoryService.addModelEditorSource(modelData.getId(), this.repositoryService.getModelEditorSource(oldModel.getId()));
        repositoryService.addModelEditorSourceExtra(modelData.getId(), this.repositoryService.getModelEditorSourceExtra(oldModel.getId()));
        response.sendRedirect(request.getContextPath() + "/modeler.html?modelId=" + modelData.getId());
    }

    /**
     * 启动流程
     * @param request
     * @return
     */
    @RequestMapping("startProcess")
    @ResponseBody
    public ResponseResult<String> startProcess(HttpServletRequest request) {
        String vacationId = request.getParameter("vacationId");
        flowInfoService.resolve(Long.valueOf(vacationId),null);
        return ResponseUtil.makeOKRsp();
    }

    /**
     * 流程图查询
     * @param request
     */
    @RequestMapping("queryFlowImg")
    public void queryFlowImg(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String flowId = request.getParameter("flowId");
        InputStream inputStream = null;
        if(StrUtil.isBlank(flowId) || StrUtil.equals("null",flowId)) {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("static/images/no_flowInfo.png");
        }else {
            Command<InputStream> cmd = new HistoryProcessInstanceDiagramCmd(flowId);
            inputStream = managementService.executeCommand(cmd);
        }
        BufferedOutputStream bout = new BufferedOutputStream(response.getOutputStream());
        try {
            if (null == inputStream) {
                inputStream = this.getClass().getResourceAsStream("/images/no_flowInfo.png");
            }
            byte b[] = new byte[1024];
            int len = inputStream.read(b);
            while (len > 0) {
                bout.write(b, 0, len);
                len = inputStream.read(b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bout.close();
            if (null != inputStream) {
                inputStream.close();
            }
        }
    }


    /**
     * 上传bpmn文件，导入系统
     * @date 2021-07-07
     * @author cool
     * repositoryService.createModelQuery().orderByCreateTime().desc().listPage
     * 表act_re_model
     * 流程列表查询
     * @return
     */
    @PostMapping("/upalodBpmnModel")
    @ResponseStatus(HttpStatus.OK)
    public void upalodBpmnModel(MultipartFile file) throws  Exception {
        //1、转换为BpmnModel模型，转换为字节数组
        byte[] bytes = file.getBytes();
        String xml = new String(bytes, StandardCharsets.UTF_8);
        //创建转换对象
        BpmnXMLConverter converter = new BpmnXMLConverter();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        // 字节方式
        XMLStreamReader reader = factory.createXMLStreamReader(new ByteArrayInputStream(xml.getBytes()));
        // 流方式
        reader = factory.createXMLStreamReader(file.getInputStream());
        //将xml文件转换成BpmnModel
        BpmnModel bpmnModel = converter.convertToBpmnModel(reader);

        //2、部署，逆向操作
        byte[] bpmnBytes = new BpmnXMLConverter().convertToXML(bpmnModel);
        String modelName="bpmnName";
        String processName = modelName + ".bpmn20.xml";
        Deployment deployment = repositoryService.createDeployment().name(modelName).addString(processName, new String(bpmnBytes,"utf-8")).deploy();
        log.info( "部署成功，部署ID=" + deployment.getId());

        //3、创建模型
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).singleResult();
        changeProcessToModel(processDefinition);

        //向流程定义表保存数据
        FlowDef flowDef = new FlowDef();
        List<Process> processes = bpmnModel.getProcesses();
        for (Process process : processes) {
            String name=process.getName();
            name=name==null?process.getId():name;
            flowDef.setFlowCode(process.getId());
            flowDef.setFlowName(name);
        }
        flowInfoService.insertFlowDef(flowDef);
    }

    public String changeProcessToModel(ProcessDefinition processDefinition) {
        String deploymentId = processDefinition.getDeploymentId();
        String modelName=processDefinition.getName();
        modelName=modelName==null?processDefinition.getKey():modelName;

        String description=processDefinition.getDescription();
        description=description==null?processDefinition.getKey()+"描述信息":description;


        Model modelData = repositoryService.newModel();
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        // 初始化Model
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode modelObjectNode = objectMapper.createObjectNode();
        modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME, modelName);
        modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, 1);
        modelObjectNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION, description);
        modelData.setMetaInfo(modelObjectNode.toString());
        modelData.setName(modelName);
        modelData.setKey(processDefinition.getKey());
        modelData.setDeploymentId(processDefinition.getDeploymentId());
        // 保存模型
        repositoryService.saveModel(modelData);

        String processDefineResourceName = null;
        String processDefineImgResourceName = null;
        // 通过deploymentId取得某个部署的资源的名称
        List<String> resourceNames = processEngine.getRepositoryService().getDeploymentResourceNames(deploymentId);
        if (resourceNames != null && resourceNames.size() > 0) {
            for (String temp : resourceNames) {
                if (temp.indexOf(".bpmn") > 0) {
                    processDefineResourceName = temp;
                }
                if (temp.indexOf(".png") > 0) {
                    processDefineImgResourceName = temp;
                }
            }
        }
        InputStream bpmnStream = processEngine.getRepositoryService().getResourceAsStream(deploymentId, processDefineResourceName);
        //创建模型关联的资源
        createModelByInputStream(bpmnStream, modelData.getId());

        bpmnStream = processEngine.getRepositoryService().getResourceAsStream(deploymentId, processDefineImgResourceName);
        createImgModelResByInputStream(bpmnStream,modelData.getId());

        return modelData.getId();
    }




    /**
     * 保存xml配置
     * @param bpmnStream
     * @param ModelID
     */
    public void createModelByInputStream(InputStream bpmnStream, String ModelID) {
        XMLInputFactory xif;
        InputStreamReader in = null;
        XMLStreamReader xtr = null;
        try {
            xif = XMLInputFactory.newFactory();
            in = new InputStreamReader(bpmnStream, "UTF-8");
            xtr = xif.createXMLStreamReader(in);
            BpmnModel bpmnModel = (new BpmnXMLConverter()).convertToBpmnModel(xtr);
            ObjectNode modelNode = new BpmnJsonConverter().convertToJson(bpmnModel);
            repositoryService.addModelEditorSource(ModelID, modelNode.toString().getBytes("UTF-8"));
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            if (xtr != null) {
                try {
                    xtr.close();
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bpmnStream != null) {
                try {
                    bpmnStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void createImgModelResByInputStream(InputStream is,String ModelID) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buff = new byte[1024];
            while ((is.read(buff)) != -1) {
                baos.write(buff);
            }
            byte[] result = baos.toByteArray();
            // 添加图片源
            this.repositoryService.addModelEditorSourceExtra(
                    ModelID, result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
