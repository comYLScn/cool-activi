package com.sunny.activiti.listener;

import cn.hutool.core.text.StrSpliter;
import com.sunny.activiti.common.util.SpringUtils;
import com.sunny.activiti.entity.FlowMain;
import com.sunny.activiti.entity.User;
import com.sunny.activiti.entity.VacationOrder;
import com.sunny.activiti.service.IFlowInfoService;
import com.sunny.activiti.service.IUserService;
import com.sunny.activiti.service.IVacationOrderService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.TaskListener;

import java.util.List;

/**
 * 需要在流程设计过程中配置usertask 监听器  start
 * @ClassName: DepartmenAuditAssginDelegate
 * @Description: 请假流程--总经理审核人指定
 * @Author: yls
 * @Date: 2021-04-12
 * @Version 1.0
 **/
@Slf4j
public class ManagerAuditAssginDelegate implements TaskListener {


    public void notify(DelegateTask delegateTask) {
        //从容器中获取服务
        IVacationOrderService vacationOrderService = SpringUtils.getBean(IVacationOrderService.class);
        IFlowInfoService flowInfoService = SpringUtils.getBean(IFlowInfoService.class);
        IUserService userService = SpringUtils.getBean(IUserService.class);

        //1、获取流程实例id
        String flowInstId=delegateTask.getProcessInstanceId();
        //2 查询流程与审批单表关联表
        FlowMain flowMain = flowInfoService.queryFlowByProcessInstanceId(Long.valueOf(flowInstId));
        //3、获取工单
        VacationOrder vacationOrder=vacationOrderService.queryVacation(flowMain.getOrderNo());
        String userId=vacationOrder.getUserId();
        //获取填写工单的用户
        User user=userService.queryUserById(userId);
        List<User> users=userService.queryUsersByOrgIdAndTag(user.getOrgId(),"manager");
        //获取填写工单用户的所属的总经理  相同orgId+tag=manager
        for(User managerUser:users){
            delegateTask.addCandidateUser(managerUser.getUserId());
        }
    }

}
