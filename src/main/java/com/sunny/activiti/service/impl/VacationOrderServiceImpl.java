package com.sunny.activiti.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sunny.activiti.common.entity.PageBean;
import com.sunny.activiti.common.entity.SysConstant;
import com.sunny.activiti.common.util.CommonUtil;
import com.sunny.activiti.entity.*;
import com.sunny.activiti.mapper.VacationOrderMapper;
import com.sunny.activiti.service.IFlowInfoService;
import com.sunny.activiti.service.ILogService;
import com.sunny.activiti.service.IUserService;
import com.sunny.activiti.service.IVacationOrderService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: VacationOrderServiceImpl
 * @Description:
 * @Author: sunt
 * @Date: 2020/6/1 17:43
 * @Version 1.0
 **/
@Service
@Slf4j
public class VacationOrderServiceImpl implements IVacationOrderService {

    @Autowired
    private VacationOrderMapper vacationOrderMapper;
    @Autowired
    private IFlowInfoService flowInfoService;
    @Autowired
    private IUserService userService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private ILogService logService;
    @Override
    @Transactional
    public void insertVacationOrder(VacationOrder vacationOrder) {
        //记录日志
        ProcessLog bean = new ProcessLog();
        User currentUser = userService.getCurrentUser();
        if(null != vacationOrder.getVacationId()) {//更新
            vacationOrderMapper.updateById(vacationOrder);
            bean.setOrderNo(vacationOrder.getVacationId());
            bean.setOperValue(currentUser.getUserName() + "修改审批单");
        }else {
            long orderNo = CommonUtil.genId();
            bean.setOrderNo(orderNo);
            vacationOrder.setVacationId(orderNo);
            vacationOrder.setVacationState(0);
            vacationOrder.setUserId(currentUser.getUserId());
            vacationOrder.setCreateTime(DateUtil.date());
//            //标记所属系统
//            vacationOrder.setSystemCode("4");
//            //标记所属业务
//            vacationOrder.setBusiType("6");
            vacationOrderMapper.insert(vacationOrder);
            bean.setOperValue(currentUser.getUserName() + "填写审批单");
        }

        logService.insertLog(bean);
    }

    @Override
    public Page<VacationOrderVo> queryVacationOrder(PageBean pageBean) {
        Page<VacationOrder> page = new Page<>(pageBean.getPage(),pageBean.getLimit());
        User currentUser = userService.getCurrentUser();
        Page<VacationOrderVo> vacationOrderPage = vacationOrderMapper.queryVacationOrder(page,currentUser.getUserId());
        return vacationOrderPage;
    }

    @Override
    public VacationOrder queryVacation(Long vacationId) {
        QueryWrapper<VacationOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("VACATION_ID",vacationId);
        return vacationOrderMapper.selectOne(queryWrapper);
    }

    @Override
    @Transactional
    public void updateState(Long vacationId, Integer state) {
        VacationOrder vacationOrder = new VacationOrder();
        vacationOrder.setVacationState(state);
        vacationOrder.setVacationId(vacationId);
        vacationOrderMapper.updateById(vacationOrder);
    }

//1、普通方式的表单提交  rejectPassModel
//    @Override
//    public boolean submitApply(Long vacationId) {
//        boolean res = true;
//        //匹配流程并指定申请人
//        Map<String, Object> variables = new HashMap<>();
//        User currentUser = userService.getCurrentUser();
//        String flowId = "";
//        //匹配流程之前查询是否已经匹配过
//        FlowMain flowMain = flowInfoService.queryFlowMainByOrderNo(vacationId);
//        if(ObjectUtil.isNull(flowMain)) {
//            variables.put("applyuser",currentUser.getUserId());
//            flowId = flowInfoService.resolve(vacationId, variables);
//        }else {
//            flowId = String.valueOf(flowMain.getFlowId());
//        }
//        if(StrUtil.isBlank(flowId)) {
//            res = false;
//            return res;
//        }
//        //流程流转，对应工作流提交成功
//        Task task = flowInfoService.queryTaskByInstId(flowId);
//        if(ObjectUtil.isNull(task)) {
//            res = false;
//            return res;
//        }
//        variables.put("subState","success");
//        log.info("------------->当前办理任务ID:{}",task.getId());
//        taskService.complete(task.getId(),variables);
//        //更新审批单状态
//        this.updateState(vacationId,SysConstant.REVIEW_STATE);
//
//        //记录日志
//        ProcessLog bean = new ProcessLog();
//        User user = userService.queryUserById(currentUser.getParentUserId());
//        bean.setOrderNo(vacationId);
//        bean.setTaskId(task.getId());
//        bean.setTaskName(task.getName());
//        bean.setTaskKey(task.getTaskDefinitionKey());
//        bean.setApprovStatu("submitApply");
//        bean.setOperValue(currentUser.getUserName() + "提交申请,待【"+user.getUserName()+"】审核");
//        logService.insertLog(bean);
//        return res;
//    }


//2、流程文件中已经指定好需要审核的用户，所以代码不用处理，不灵活  rejectPassModelWithID
//    @Override
//    public boolean submitApply(Long vacationId) {
//        boolean res = true;
//        //匹配流程并指定申请人
//        Map<String, Object> variables = new HashMap<>();
//        User currentUser = userService.getCurrentUser();
//        String flowId = "";
//        //匹配流程之前查询是否已经匹配过
//        FlowMain flowMain = flowInfoService.queryFlowMainByOrderNo(vacationId);
//        if(ObjectUtil.isNull(flowMain)) {
//            //当前提交申请单的用户
//            variables.put("applyuser",currentUser.getUserId());
//            flowId = flowInfoService.resolve(vacationId, variables);
//        }else {
//            flowId = String.valueOf(flowMain.getFlowId());
//        }
//        if(StrUtil.isBlank(flowId)) {
//            res = false;
//            return res;
//        }
//        //流程流转，对应工作流提交成功
//        Task task = flowInfoService.queryTaskByInstId(flowId);
//        if(ObjectUtil.isNull(task)) {
//            res = false;
//            return res;
//        }
//        //当前提交申请单的用户
//        variables.put("applyuser",currentUser.getUserId());
//        //TODO 添加用户身份标签
//        variables.put("tag",currentUser.getTag());
//        //状态成功，跳转到下一个节点
//        variables.put("subState","success");
//
//        log.info("------------->当前办理任务ID:{}",task.getId());
//        taskService.complete(task.getId(),variables);
//        //更新审批单状态
//        this.updateState(vacationId,SysConstant.REVIEW_STATE);
//
//        //记录日志
//        ProcessLog bean = new ProcessLog();
//        User user = userService.queryUserById(currentUser.getParentUserId());
//        bean.setOrderNo(vacationId);
//        bean.setTaskId(task.getId());
//        bean.setTaskName(task.getName());
//        bean.setTaskKey(task.getTaskDefinitionKey());
//        bean.setApprovStatu("submitApply");
//        bean.setOperValue(currentUser.getUserName() + "提交申请,待【"+user.getUserName()+"】审核");
//        logService.insertLog(bean);
//        return res;
//    }

    //3、 由流程文件中的listener中的动态指定审核人
    @Override
    public boolean submitApply(Long vacationId) {
        boolean res = true;
        //匹配流程并指定申请人
        Map<String, Object> variables = new HashMap<>();
        User currentUser = userService.getCurrentUser();
        String flowId = "";
        //匹配流程之前查询是否已经匹配过
        FlowMain flowMain = flowInfoService.queryFlowMainByOrderNo(vacationId);
        if(ObjectUtil.isNull(flowMain)) {
            //当前提交申请单的用户
            variables.put("applyuser",currentUser.getUserId());
            flowId = flowInfoService.resolve(vacationId, variables);
        }else {
            flowId = String.valueOf(flowMain.getFlowId());
        }
        if(StrUtil.isBlank(flowId)) {
            res = false;
            return res;
        }
        //流程流转，对应工作流提交成功
        Task task = flowInfoService.queryTaskByInstId(flowId);
        if(ObjectUtil.isNull(task)) {
            res = false;
            return res;
        }
        //当前提交申请单的用户
        variables.put("applyuser",currentUser.getUserId());
        //添加用户身份标签
        variables.put("tag",currentUser.getTag());


        log.info("------------->当前办理任务ID:{}",task.getId());
        taskService.complete(task.getId(),variables);
        //更新审批单状态
        this.updateState(vacationId,SysConstant.REVIEW_STATE);

        //记录日志
        ProcessLog bean = new ProcessLog();
        User user = userService.queryUserById(currentUser.getParentUserId());
        bean.setOrderNo(vacationId);
        bean.setTaskId(task.getId());
        bean.setTaskName(task.getName());
        bean.setTaskKey(task.getTaskDefinitionKey());
        bean.setApprovStatu("submitApply");
        bean.setOperValue(currentUser.getUserName() + "提交申请,待【"+user.getUserName()+"】审核");
        logService.insertLog(bean);
        return res;
    }

    //提交申请单，由前端指定表单
    @Override
    public  boolean submitApplyForUser(Long vacationId, String userIdStr){
        boolean res = true;
        //匹配流程并指定申请人
        Map<String, Object> variables = new HashMap<>();
        User currentUser = userService.getCurrentUser();
        String flowId = "";
        //匹配流程之前查询是否已经匹配过
        FlowMain flowMain = flowInfoService.queryFlowMainByOrderNo(vacationId);
        if(ObjectUtil.isNull(flowMain)) {
            //当前提交申请单的用户
            variables.put("applyuser",currentUser.getUserId());
            flowId = flowInfoService.resolve(vacationId, variables);
        }else {
            flowId = String.valueOf(flowMain.getFlowId());
        }
        if(StrUtil.isBlank(flowId)) {
            res = false;
            return res;
        }
        //流程流转，对应工作流提交成功
        Task task = flowInfoService.queryTaskByInstId(flowId);
        if(ObjectUtil.isNull(task)) {
            res = false;
            return res;
        }
        //当前提交申请单的用户
        variables.put("applyuser",currentUser.getUserId());
        //添加用户身份标签
        variables.put("tag",currentUser.getTag());
        //添加指定人,提交申请单一定会指定审核人可以不做判断
        variables.put("userIds",userIdStr);

        log.info("------------->当前办理任务ID:{}",task.getId());
        taskService.complete(task.getId(),variables);
        //更新审批单状态
        this.updateState(vacationId,SysConstant.REVIEW_STATE);

        //记录日志
        ProcessLog bean = new ProcessLog();
        User user = userService.queryUserById(currentUser.getParentUserId());
        bean.setOrderNo(vacationId);
        bean.setTaskId(task.getId());
        bean.setTaskName(task.getName());
        bean.setTaskKey(task.getTaskDefinitionKey());
        bean.setApprovStatu("submitApply");
        bean.setOperValue(currentUser.getUserName() + "提交申请,待【"+user.getUserName()+"】审核");
        logService.insertLog(bean);
        return res;
    }


    @Override
    @Transactional
    public void delVacation(Long vacationId) {
        this.updateState(vacationId, SysConstant.OBSOLETE_STATE);
        //记录日志
        User currentUser = userService.getCurrentUser();
        ProcessLog bean = new ProcessLog();
        User user = userService.queryUserById(currentUser.getParentUserId());
        bean.setOrderNo(vacationId);
        bean.setApprovStatu("DELETE");
        bean.setOperValue(currentUser.getUserName() + "删除审批单");
        logService.insertLog(bean);
    }

}
